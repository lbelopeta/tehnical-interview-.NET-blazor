using System.ComponentModel.DataAnnotations;

namespace Web.Features.Billing.Models
{
    public class CheckoutFormModel
    {
        [Required, MinLength(12)]  public string CreditCardNumber { get; set; }
        [Required, MinLength(5)]  public string ExpirationDate { get; set; }
        [Required, MinLength(3)]  public string SecurityCode { get; set; }
    }
}
