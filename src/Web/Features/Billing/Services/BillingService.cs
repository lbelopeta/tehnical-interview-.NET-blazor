﻿using Web.Features.Billing.Abstractions;
using Web.Features.Billing.Models;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Data.Context;
using Microsoft.EntityFrameworkCore;

namespace Web.Features.Billing.Services
{
    public sealed class BillingService : IBillingService
    {
        private readonly MyContext _dbContext;

        public BillingService(MyContext dbContext)
        {
            _dbContext = dbContext;
        }
        
        /// <inheritdoc/>
        public Task<bool> HasAccessAsync(BillingContext MyContext, CancellationToken cancellationToken)
        {
            var user = MyContext.User;
            var report = MyContext.Report;
            var taxYear = report.TaxYear;
            
            //fetch all billing tiers which can be applied to this report
            var applicableBillingTiers = user.Purchases
                .Where(p => p.TaxYear == taxYear)
                .Select(p => p.Tier);

            var hasAccess = applicableBillingTiers.Any(t => t.Threshold > report.TradeCount);
            return Task.FromResult(hasAccess);
        }

        /// <inheritdoc/>
        public async Task<int> CalculatePriceAsync(BillingContext MyContext, CancellationToken cancellationToken)
        {
            var reportTradeCount = MyContext.Report.TradeCount;

            //fetch cheapest billing tier required for viewing specified report
            var cheapestBillingTier = await _dbContext.BillingTiers
                .AsQueryable()
                .Where(bt => bt.Threshold > reportTradeCount)
                .OrderBy(bt => bt.FullAmountInCents)
                .FirstOrDefaultAsync(cancellationToken);
            
            if (cheapestBillingTier == null)
                throw new InvalidOperationException("No applicable billing tier was found");

            return cheapestBillingTier.FullAmountInCents;
        }
    }
}
