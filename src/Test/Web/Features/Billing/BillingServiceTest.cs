using System;
using System.Threading.Tasks;
using Data.Context;
using Data.Entities;
using FluentAssertions;
using NUnit.Framework;
using Web.Features.Billing.Models;
using Web.Features.Billing.Services;
using static Test.Extensions.InMemoryContextExtensions;

namespace Test.Web.Features.Billing
{
    [TestFixture]
    public class BillingServiceTest
    {
        #region Setup

        private sealed class Setup : IDisposable
        {
            public Setup(MyContext dbContext)
            {
                Db = dbContext;
            }

            public MyContext Db;

            public void Dispose()
            {
                Db?.Dispose();
            }
        }

        private Setup Initialize()
        {
            var db = CreateTestContext();
            return new Setup(db);
        }

        #endregion

        [Test]
        public async Task HasAccessAsync_Should_Return_True_When_User_Has_Applicable_Billing_Plan()
        {
            // Arrange
            using var deps = Initialize();

            var dbUser = User.Create("Test User", "dev@technical.tax");
            var dbBillingTier = BillingTier.Create("Test billing tier", int.MaxValue, 5000);
            var dbPurchase = Purchase.Create(2020, DateTimeOffset.Now, 5000, dbBillingTier);
            var dbReport = Report.Create(2020, 20, 1, 1, 0);

            dbUser.Purchases.Add(dbPurchase);
            dbUser.Reports.Add(dbReport);

            deps.Db.Users.Add(dbUser);
            deps.Db.BillingTiers.Add(dbBillingTier);
            deps.Db.SaveChanges();

            var service = new BillingService(deps.Db);
            var billingContext = new BillingContext(dbUser, dbReport);

            // Act
            var result = await service.HasAccessAsync(billingContext, default);

            // Assert
            result.Should().BeTrue();
        }

        [Test]
        public async Task HasAccessAsync_Should_Return_False_When_User_Does_Not_Have_Applicable_Billing_Plan()
        {
            // Arrange
            using var deps = Initialize();

            var dbUser = User.Create("Test User", "dev@technical.tax");
            var dbBillingTier = BillingTier.Create("Test billing tier", 10, 5000);
            var dbPurchase = Purchase.Create(2020, DateTimeOffset.Now, 5000, dbBillingTier);
            var dbReport = Report.Create(2020, 20, 1, 1, 0);

            dbUser.Purchases.Add(dbPurchase);
            dbUser.Reports.Add(dbReport);

            deps.Db.Users.Add(dbUser);
            deps.Db.BillingTiers.Add(dbBillingTier);
            deps.Db.SaveChanges();


            var service = new BillingService(deps.Db);

            var billingContext = new BillingContext(dbUser, dbReport);


            // Act
            var result = await service.HasAccessAsync(billingContext, default);

            // Assert
            result.Should().BeFalse();
        }

        [Test]
        public async Task CalculatePriceAsync_Should_Return_Cheapest_Price()
        {
            // Arrange
            using var deps = Initialize();

            var dbUser = User.Create("Test User", "dev@technical.tax");
            var dbBillingTier = BillingTier.Create("Test billing tier", int.MaxValue, 5000);
            var dbBillingTierExpensive = BillingTier.Create("Test billing tier2", int.MaxValue, 50000);
            var dbReport = Report.Create(2020, 0, 1, 1, 0);
            dbUser.Reports.Add(dbReport);


            deps.Db.Users.Add(dbUser);
            deps.Db.BillingTiers.AddRange(dbBillingTier, dbBillingTierExpensive);
            deps.Db.SaveChanges();

            var service = new BillingService(deps.Db);
            var billingContext = new BillingContext(dbUser, dbReport);

            // Act
            var result = await service.CalculatePriceAsync(billingContext, default);
            // Assert
            result.Should().Be(5000);
        }

        [Test]
        public void CalculatePriceAsync_Should_Throw_When_There_Are_No_Applicable_Billing_Tiers()
        {
            // Arrange
            using var deps = Initialize();

            var dbUser = User.Create("Test User", "dev@technical.tax");
            var dbReport = Report.Create(2020, 0, 1, 1, 0);
            dbUser.Reports.Add(dbReport);

            deps.Db.Users.Add(dbUser);
            deps.Db.SaveChanges();

            var service = new BillingService(deps.Db);
            var billingContext = new BillingContext(dbUser, dbReport);

            // Act
            Func<Task> action = async () => await service.CalculatePriceAsync(billingContext, default);
            action.Invoke();

            // Assert
            action.Should().Throw<InvalidOperationException>();
        }
    }
}
